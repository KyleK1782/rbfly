#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Basic types for RabbitMQ Streams implementation.
"""

import typing as tp
from collections import deque

from ..amqp import MessageCtx

MessageQueue = deque[MessageCtx]

class PublishError(tp.NamedTuple):
    """
    Error information received from RabbitMQ Streams broker after
    publishing a message.
    """
    publisher_id: int
    errors: tuple[tuple[int, int], ...]

# vim: sw=4:et:ai
