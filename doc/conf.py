import sys
import os.path
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath('..'))

import rbfly

extensions = [
    'sphinx.ext.autodoc', 'sphinx.ext.autosummary', 'sphinx.ext.doctest',
    'sphinx.ext.todo', 'sphinx.ext.viewcode'
]
project = 'RbFly'
source_suffix = '.rst'
master_doc = 'index'

title = 'RbFly - a library for RabbitMQ Streams using Python asyncio'
version = release = rbfly.__version__
copyright = 'Artur Wroblewski'

epub_basename = 'RbFly - {}'.format(version)
epub_author = 'Artur Wroblewski'

todo_include_todos = True

html_theme = 'sphinx_rtd_theme'
html_static_path = ['static']
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_domain_indices = False

latex_fout = 'rbfly-{}.tex'.format(version)
latex_documents = [
    ('index', latex_fout, title, 'RbFly team', 'manual', True)
]
latex_elements = {
    'papersize': 'a4paper',
}
latex_domain_indices = False

autodoc_type_aliases = {
    'AMQPBody': 'rbfly.streams.types.AMQPBody'
}

# vim: sw=4:et:ai
