Introduction
============
RbFly is a library for `RabbitMQ Streams <https://www.rabbitmq.com/streams.html>`_
using `Python asyncio <https://docs.python.org/3/library/asyncio.html>`_.

Features
--------
The library is designed and implemented with the following features in mind

1. Simple and flexible API.
2. Use of AMQP 1.0 message format to enable interoperability between
   RabbitMQ Streams clients.
3. Performance by implementing critical sections of code using `Cython
   <https://cython.readthedocs.io/en/latest/>`_, avoiding copying of data,
   and testing. The testing involves use of asyncio drop-in replacement
   `uvloop <https://uvloop.readthedocs.io/>`_, which improves performance
   of Python asyncio based applications.
4. Auto reconnection to RabbitMQ broker with lazily created connection
   objects.

.. note::
   AMQP 1.0 support is work in progress. See :ref:`stream-message-type` for
   details.

Requirements
------------
RbFly requires

- Python 3.10
- RabbitMQ 3.10.0
- Cython 0.29.x (for development)

Installation
------------
Install or upgrade RbFly from `PyPi <https://pypi.org/project/rbfly/>`_
with `pip command
<https://packaging.python.org/en/latest/tutorials/installing-packages/#installing-from-pypi>`_::

    $ pip install -U rbfly

License
-------
RbFly library is licensed under terms of `GPL license, version 3
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_. As stated in the license,
there is no warranty, so any usage is on your own risk.

Acknowledgements
----------------
The design and implementation of RbFly library is inspired by other
projects

- `asyncpg <https://magicstack.github.io/asyncpg/current/>`_
- `rstream <https://github.com/qweeze/rstream>`_
- `aioredis <https://aioredis.readthedocs.io/en/latest/>`_
- `aiokafka <https://aiokafka.readthedocs.io/en/stable/>`_

.. vim: sw=4:et:ai
