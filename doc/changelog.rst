Changelog
=========
2022-11-11, ver. 0.5.3
----------------------
- use recursive definition for container amqp types, i.e. sequence can
  contain another sequence or a dictionary
- documentation improvements

2022-11-01, ver. 0.5.2
----------------------
- fix encoding of binary data in containers, i.e. list of elements having
  bytes type

2022-10-16, ver. 0.5.1
----------------------
- fix deadlock of rabbitmq streams protocol publish method when no
  messages are sent

2022-10-09, ver. 0.5.0
----------------------
- implement rabbitmq streams batching publisher with memory protection and
  concurrently working from multiple asynchronous coroutines
- send heartbeat if there is no communication from rabbitmq streams broker
  and restart connection if no response

2022-09-12, ver. 0.4.2
----------------------
- allow sending messages via a streams publisher from multiple coroutines
- improve rabbitmq streams connection decorator type annotations

2022-08-16, ver. 0.4.1
----------------------
- minor fixes post 0.4.0 release

2022-08-16, ver. 0.4.0
----------------------
- add support for encoding and decoding of timestamp and UUID values in
  amqp 1.0 codec
- add support for decoding of message annotations and application
  properties of amqp message
- fix amqp decoder when dealing with unsigned char values (i.e. size of
  a byte string)
- fix storing of rabbitmq streams timestamp in message context object

2022-05-23, ver. 0.3.0
----------------------
- api change: do not store stream offset value in stream subscription
  method to give more control over the action to a programmer
- implement rabbitmq streams client method to store stream offset value
- implement function to get message context for a received stream message

2022-05-13, ver. 0.2.0
----------------------
- change publisher reference name scheme to ``<hostname>/<pid>``
- allow to set publisher reference name when creating a stream publisher
- improve handling of rabbitmq streams disconnections
- increase sleep time when retrying stream subscription or rabbitmq streams
  broker reconnection (current sleep time is too aggressive with simple
  broker restart)

2022-04-13, ver. 0.1.1
----------------------
- fix handling of multiple disconnection requests, i.e. when multiple
  coroutines share a client
- properly propagate asynchronous generators via the connection decorator

2022-04-06, ver. 0.1.0
----------------------
- improvements to decoding of amqp messages
- fix compilation issues on macos and freebsd operating systems

2022-03-25, ver. 0.0.4
----------------------
- add support for value of type list in amqp messages

2022-03-24, ver. 0.0.3
----------------------
- add support for values of type boolean, integer, float, and dictionaries
  in amqp messages

2022-01-24, ver. 0.0.2
----------------------
- fix encoding of messages

2022-01-16, ver. 0.0.1
----------------------
- initial release

.. vim: sw=4:et:ai
