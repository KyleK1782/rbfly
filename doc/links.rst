Links
=====

RbFly
-----
1. Website: https://wrobell.dcmod.org/rbfly/index.html
2. Report bugs: https://gitlab.com/wrobell/rbfly/-/issues
3. Source code: https://gitlab.com/wrobell/rbfly

RabbitMQ Streams
----------------
1. RabbitMQ Streams: https://www.rabbitmq.com/streams.html
2. RabbitMQ Streams Java library: https://rabbitmq.github.io/rabbitmq-stream-java-client/stable/htmlsingle/ 
3. Discussions: https://groups.google.com/g/rabbitmq-users/

.. vim: sw=4:et:ai
