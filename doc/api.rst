RabbitMQ Streams API
====================

.. autosummary::
   :nosignatures:

   rbfly.streams.streams_client
   rbfly.streams.connection
   rbfly.streams.get_message_ctx
   rbfly.streams.StreamsClient
   rbfly.streams.Publisher
   rbfly.streams.PublisherBatchMem
   rbfly.streams.PublisherBatch
   rbfly.streams.MessageCtx
   rbfly.streams.Offset
   rbfly.types.AMQPBody
   rbfly.streams.Symbol


.. autofunction:: rbfly.streams.streams_client
.. autofunction:: rbfly.streams.connection
.. autofunction:: rbfly.streams.get_message_ctx

.. autoclass:: rbfly.streams.StreamsClient
   :members:

.. autoclass:: rbfly.streams.Publisher
   :members:

.. autoclass:: rbfly.streams.PublisherBatchMem
   :members:

.. autoclass:: rbfly.streams.PublisherBatch
   :members:

.. autoclass:: rbfly.streams.MessageCtx

.. autoclass:: rbfly.streams.Offset
   :members:

.. autodata:: rbfly.types.AMQPBody

   The type alias: :py:data:`rbfly.streams.AMQPBody`.

.. autoclass:: rbfly.streams.Symbol

.. vim: sw=4:et:ai
