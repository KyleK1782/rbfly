RbFly - RabbitMQ Client Library
===============================
RbFly is a library for `RabbitMQ Streams <https://www.rabbitmq.com/streams.html>`_
using `Python asyncio <https://docs.python.org/3/library/asyncio.html>`_.

This documentation can be downloaded in `PDF format <rbfly-0.5.3.pdf>`_.

RbFly library is licensed under terms of `GPL license, version 3
<https://www.gnu.org/licenses/gpl-3.0.en.html>`_. As stated in the license,
there is no warranty, so any usage is on your own risk.

.. toctree::
   :caption: Table of Contents
   :maxdepth: 2

   intro
   client
   api
   links
   changelog

* :ref:`genindex`

.. vim: sw=4:et:ai
