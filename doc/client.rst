RabbitMQ Streams Client
=======================
Quick Start
-----------
The example below demonstrates various RbFly library features, which can be
utilized in an application using `RabbitMQ Streams
<https://www.rabbitmq.com/streams.html>`_:

- creating RabbitMQ Streams client using connection URI
- creating a stream (`demo` coroutine)
- creating a stream publisher and publishing messages to a stream
  (`send_data` coroutine)
- subscribing to a stream and receiving messages from a stream
  (`receive_data` coroutine)
- the script continues to run if RabbitMQ Streams broker stops and starts
  again

.. literalinclude:: ../scripts/rbfly-demo
   :language: python
   :start-at: import asyncio
   :end-at: asyncio.run

The source code of the demo can be downloaded from `RbFly code repository
<https://gitlab.com/wrobell/rbfly/-/blob/master/scripts/rbfly-demo>`_.

The following sections discuss the features of RbFly library in more
detail.

Connecting and Managing Connection
----------------------------------
Use :py:func:`~rbfly.streams.streams_client` function to declare connection
to RabbitMQ Streams broker. The function creates RabbitMQ Streams client
object, which is used to :ref:`create streams<stream-create>`,
:ref:`publish messages<stream-publish>` to a stream, and
:ref:`subscribe<stream-subscribe>` to a stream.

Whenever an action of a client requires an interaction with RabbitMQ
Streams broker, a connection is created or existing connection is reused.

When a broker is restarted (i.e. after crash), then RbFly will reconnect
the client and retry an interrupted action.

Properly closing a connection to RabbitMQ Streams broker requires use of
:py:func:`~rbfly.streams.connection` decorator. Use it to decorate a Python
asynchronous coroutine function. The decorator closes a connection to
a broker when the decorated coroutine exits.

The example below is a skeleton of a Python program creating RabbitMQ
Streams broker client, implementing asynchronous coroutine to interact with
the broker, and to close connection on the coroutine exit::

    import asyncio
    import rbfly.streams as rbs

    @rbs.connection
    async def streams_app(client: rbs.StreamsClient) -> None:
        ...
        # interaction with RabbitMQ Streams broker
        ...

    client = rbs.streams_client('rabbitmq-stream://guest:guest@localhost')
    asyncio.run(streams_app(client))

Connection URI Specification
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The string specifying RabbitMQ Streams connection is URI in the format::

     rabbitmq-stream://[user[:password]@][host[:port][/vhost]]

where

user
    Name of user connecting to RabbitMQ Streams broker.
password
    Password of the user.
host
    RabbitMQ Streams broker hostname.
port
    RabbitMQ Streams broker port.
vhost
    Virtual host of RabbitMQ Streams broker.

.. _stream-create:

Creating and Deleting Streams
-----------------------------
Use RabbitMQ broker `rabbitmqadmin`
`command-line tool <https://www.rabbitmq.com/management-cli.html>`_ to create
a stream::

    $ rabbitmqadmin declare queue name=a-stream queue_type=stream arguments='{"x-max-age": "24h"}'

A stream can be deleted, like any other RabbitMQ queue, with `rabbitmqctl`
command-line tool::

    $ rabbitmqctl delete_queue a-stream

Optionally, RbFly supports creating streams programmatically with
:py:meth:`~rbfly.streams.StreamsClient.create_stream` method::

    await client.create_stream('a-stream')

.. _stream-publish:

Publishing Messages to a Stream
-------------------------------
To send messages to a RabbitMQ stream create a RabbitMQ Streams publisher
with :py:meth:`~rbfly.streams.StreamsClient.publisher` method. There are
multiple types of publishers implemented by the following classes

:py:class:`rbfly.streams.Publisher`
    Send a message and wait for RabbitMQ Streams broker to confirm that the
    message is received. Multiple asynchronous coroutines can send messages
    with the same publisher concurrently. This is the slowest way of
    sending messages. It is the default RbFly publisher.

:py:class:`rbfly.streams.PublisherBatchMem`
    Batch multiple messages to send them to a stream, then send all
    messages with a flush method. Batch method blocks if a limit of
    messages is reached, so an application does not run of out memory.
    Batching and flushing can be performed concurrently from different
    asynchronous coroutines. This is faster method of sending messages, than
    the previous one.

:py:class:`rbfly.streams.PublisherBatch`
    Batch multiple messages, then send all messages with the flush method.
    An application needs to flush messages to avoid running out of memory.
    Use it only when messages can be batched and flushed in a sequence,
    i.e. from the same asynchronous coroutine. This is the fastest method
    of sending messages to a stream, but provides no coordination between
    batch and flush methods.

.. note::
   RabbitMQ Streams publisher uses publisher reference name for message
   deduplication. By default, a publisher name is set using
   ``<hostname>/<pid>`` format.

   RbFly does not support sending messages with custom publishing
   identifier, at the moment. Therefore, publisher name needs to be unique
   per stream or messages will be lost.

   If the naming scheme is not sufficient, then publisher reference name
   can be overriden when creating a publisher.

Sending Single Message
^^^^^^^^^^^^^^^^^^^^^^
To send messages one by one simply create a RabbitMQ Streams publisher and
send messages with :py:meth:`~rbfly.streams.Publisher.send` asynchronous
coroutine::

    async with client.publisher('stream-name') as publisher:
        message = 'hello'
        await publisher.send(message)

The coroutine sends a message and waits for RabbitMQ Streams broker for the
published message confirmation.

Batch Publishing with Memory Protection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
To send messages in batch mode with memory protection create publisher
using :py:class:`~rbfly.streams.PublisherBatchMem` class. Enqueue each
message with :py:meth:`~rbfly.streams.PublisherBatchMem.batch` asynchronous
coroutine method::

    async with client.publisher('stream-name', cls=PublisherBatchMem) as publisher:
        await publisher.batch('hello 1')
        await publisher.batch('hello 2')
        await publisher.flush()

Use :py:meth:`~rbfly.streams.PublisherBatchMem.flush` asynchronous coroutine to
send the messages to RabbitMQ Streams broker and wait for published
messages confirmation.

Batch Publishing
^^^^^^^^^^^^^^^^
To send messages in batch mode create publisher using
:py:class:`~rbfly.streams.PublisherBatch` class. Enqueue each message with
:py:meth:`~rbfly.streams.PublisherBatch.batch` method::

    async with client.publisher('stream-name', cls=PublisherBatch) as publisher:
        publisher.batch('hello 1')
        publisher.batch('hello 2')
        await publisher.flush()

Use :py:meth:`~rbfly.streams.PublisherBatch.flush` asynchronous coroutine to
send the messages to RabbitMQ Streams broker and wait for published
messages confirmation.

.. _stream-message-type:

Message Data Types
^^^^^^^^^^^^^^^^^^
Message body, sent to a RabbitMQ stream, is any Python object having one of
the types

bytes
    A data section of AMQP message containing opaque binary data. It can
    carry any binary message encoded with third-party encoders like
    `MessagePack <https://msgpack.org/>`_.
str
    An AMQP message with string value encoded with Unicode.
bool
    An AMQP message with boolean value (true or false).
int
    An AMQP message with integer value. The value is encoded with AMQP
    type as defined by the table

    ===================== ==============
    Value Range           AMQP Type Name
    ===================== ==============
    [-2 ^ 31, 2 ^ 31 - 1]      int
    [-2 ^ 63, 2 ^ 63 - 1]      long
     [2 ^ 63, 2 ^ 64 - 1]      ulong
    ===================== ==============
float
    An AMQP message with value of AMQP type double. AMQP type float is
    parsed as value of Python type float, as well.
sequence
    An AMQP message with value of AMQP type list. Python list and tuples
    are supported when serializing to AMQP format. Always decoded into
    a Python list.
dict
    An AMQP message with value of AMQP type map. The order of key-value
    pairs in a map is preserved on encoding and decoding. In AMQP standard,
    the order of key-value pairs is semantically important, but this is not
    the case for Python dictionary.
datetime.datetime
    An AMQP message with value of AMQP type timestamp. Unaware datetime
    object is encoded as UTC timestamp value. Decoded datetime object has
    UTC timezone.
uuid.UUID
    An AMQP message with an universally unique identifier as defined by
    RFC-4122 section 4.1.2.
:py:class:`~rbfly.streams.Symbol`
    Symbolic value from a constrained domain. The value is an ASCII string.

.. _stream-subscribe:

Subscribing to a Stream
-----------------------
Reading Messages
^^^^^^^^^^^^^^^^
Subscribe to a RabbitMQ stream with :py:meth:`~rbfly.streams.StreamsClient.subscribe`
method. The method is an asynchronous iterator, which yields stream
messages::

    async for msg in client.subscribe('stream-name'):
        print(msg)

The method accepts the offset parameter. This is described in more detail
in :ref:`stream-offset` section.

Message Context
^^^^^^^^^^^^^^^
A RabbitMQ Streams message has a set of properties like offset or
timestamp. Also, AMQP 1.0 message, beside message body, can have additional
data attached like header or application properties.

The additional information is available via
:py:class:`~rbfly.streams.MessageCtx` object, which can be retrieved with
:py:func:`~rbfly.streams.get_message_ctx` function, for example::

    async for msg in client.subscribe('stream-name'):
        print(get_message_ctx().stream_offset)

.. _stream-offset:

Offset Specification
^^^^^^^^^^^^^^^^^^^^
.. automodule:: rbfly.streams.offset

Offset Reference
^^^^^^^^^^^^^^^^
RabbitMQ Streams supports storing an offset value in a stream using a string
reference, and receiving an offset value using the reference.

Use :py:meth:`offset reference specification <rbfly.streams.Offset.reference>`
to read messages from a stream starting after stored offset value::

    messages = client.subscribe('stream-name', offset=Offset.reference('stream-ref'))
    try:
        async for msg in messages:
            print msg
    finally:
        await client.write_offset('stream-name', 'stream-ref')

In the example above, RbFly library performs the following actions

- read offset value, stored for offset reference string `stream-ref`, from
  RabbitMQ Streams broker
- start reading messages of stream `stream-name` with the
  offset value increased by one (start reading *after* stored value)
- keep offset value of last received message in memory

The example saves offset value with :py:meth:`~rbfly.streams.StreamsClient.write_offset`
method. By default, the method saves offset value of last stream message.
Custom offset value, can also be specified, see
:py:meth:`the method documentation <rbfly.streams.StreamsClient.write_offset>`
for details.

.. note::
   How and when offset value shall be saved is application dependant.

.. vim: sw=4:et:ai
