#!/usr/bin/env python3
#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
RbFly scripting utilities.
"""

import typing as tp

Printer: tp.TypeAlias = tp.Callable[[float, float], None]

def print_pretty(
        name: str,
        type: str,
        count: int,
        batch_size: int,
        time_start: float,
        time_end: float,
) -> None:
    """
    Pretty print performance result message.
    """
    msg = 'published messages: type={}, n={:.2f} mln, batch={},' \
        ' speed={:,.2f} msg/sec'
    cnt = count / (time_end - time_start)
    print(msg.format(type, count / 1e6, batch_size, cnt))

def print_csv(
        name: str,
        type: str,
        count: int,
        batch_size: int,
        time_start: float,
        time_end: float,
) -> None:
    """
    Print performance result message in CSV format.
    """
    cnt = count / (time_end - time_start)
    print('{},{},{},{},{}'.format(name, type, count, batch_size, cnt))

# vim: sw=4:et:ai
