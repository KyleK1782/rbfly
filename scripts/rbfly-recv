#!/usr/bin/env python3
#
# rbfly - a library for RabbitMQ Streams using Python asyncio
#
# Copyright (C) 2021-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Script to read data from RabbitMQ stream and show some statistics.
"""

import argparse
import asyncio
import dataclasses as dtc
import logging
import uvloop
import sys
from collections.abc import Sized
from collections import Counter
from datetime import datetime

import rbfly.streams as rbs

logger = logging.getLogger(__name__)

@dtc.dataclass
class State:
    counter: Counter[str]
    message: rbs.AMQPBody

async def print_stats(state: State) -> None:
    """
    Print stats about messages.
    """
    prev_count = 0
    while True:
        await asyncio.sleep(1)
        count = state.counter['messages']
        if count > prev_count:
            prev_count = count
            print('time:', datetime.now())
            print('  number of messages: {}'.format(count))
            print('  total size of messages: {}'.format(state.counter['size']))
            print('  last message: {!r:.80}'.format(state.message))
            print()

@rbs.connection
async def receive_data(
        client: rbs.StreamsClient,
        stream: str,
        offset: rbs.Offset,
        amqp: bool,
        state: State,
        *,
        create_stream: bool=True,
    ) -> None:
    """
    Read messages from RabbitMQ stream.
    """
    if create_stream:
        await client.create_stream(stream)
    try:
        async for msg in client.subscribe(stream, offset=offset, amqp=amqp):
            state.counter['messages'] += 1
            state.counter['size'] += len(msg) if isinstance(msg, Sized) else 1
            state.message = msg
    finally:
        if offset.type == rbs.OffsetType.REFERENCE:
            assert isinstance(offset.value, str)
            await client.write_offset(stream, offset.value)

async def start(
        client: rbs.StreamsClient,
        stream: str,
        offset: rbs.Offset,
        amqp: bool,
        *,
        create_stream: bool=True,
    ) -> None:
    """
    Start RabbitMQ stream reader and stats printer.
    """
    state = State(Counter[str](messages=0), b'')

    tasks = (
        receive_data(
            client, stream, offset, amqp, state, create_stream=create_stream
        ),
        print_stats(state),
    )
    await asyncio.gather(*tasks)

parser = argparse.ArgumentParser()
parser.add_argument(
    '--verbose', action='store_true',
    help='explain what is being done'
)
parser.add_argument(
    '-c', '--create-stream', action='store_true', default=False,
    help='create stream'
)
parser.add_argument(
    '--offset', type=int, default=None,
    help='Stream offset value to start reading from'
)
parser.add_argument(
    '--offset-ref', type=str, default=None,
    help='Stream offset reference name'
)
parser.add_argument(
    '-t', '--type', choices=['amqp', 'binary'], default='amqp',
    help='Type of message sent to the stream: AMQP or binary opaque data'
)
parser.add_argument('uri', help='RabbitMQ Streams broker URI')
parser.add_argument('stream', help='Stream name')

args = parser.parse_args()

level = logging.DEBUG if args.verbose else logging.INFO
logging.basicConfig(level=level)

client = rbs.streams_client(args.uri)
amqp = args.type == 'amqp'

match (args.offset, args.offset_ref):
    case (None, None):
        offset = rbs.Offset.NEXT
    case (int(n), None):
        offset = rbs.Offset.offset(n)
    case (None, str(n)):
        offset = rbs.Offset.reference(n)
    case _:
        sys.exit(
            'rbfly-recv: Specify offset value, or offset reference,'
            ' or none of the options'
        )

uvloop.install()

task = start(
    client, args.stream, offset, amqp, create_stream=args.create_stream
)
asyncio.run(task)

# vim: sw=4:et:ai
