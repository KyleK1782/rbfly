# commands:
#
# 	doc
# 		generate documentation
# 	doc-upload
# 		upload documentation
# 	check-code
# 		run code quality analysis
# 	check-type
# 		run static type analysis
# 	test
# 		run unit tests
# 	clean
# 		remove build directories, rendered documentation, and other build
# 		related artefacts
#
# project configuration:
#
#   SCRIPTS
#       list of scripts, which are part of the project
#   DOC_DEST 
#       where to upload the documentation
#   PYLINT_SCORE
#       Pylint minimum score to *not* fail `check-code` target

.PHONY = doc doc-upload check test clean

PROJECT = $(shell grep '^name' setup.cfg | awk -F= '{gsub(/ /, "", $$2); print $$2}')
VERSION = $(shell grep '^version' setup.cfg | awk -F= '{gsub(/ /, "", $$2); print $$2}')
MODULE = $(shell echo $(PROJECT) | sed 's/-//g')
PKG_INFO = $(shell echo $(PROJECT) | sed 's/-/_/g').egg-info/PKG-INFO
EGG = $(shell echo $(PROJECT) | sed 's/-/_/g')

export PYTHONPATH=.

RSYNC = rsync -cav \
	--exclude=\*~ --exclude=.\* \
	--delete-excluded --delete-after \
	--no-owner --no-group \
	--progress --stats

doc: .stamp-sphinx

doc-upload:
	$(RSYNC) build/doc/ $(DOC_DEST)

clean:
	rm -rf .$(PROJECT)*stamp .sphinx-stamp
	rm -rf $(PROJECT).egg-info
	rm -rf build/doc build/latex

check-type: .stamp-$(PROJECT)-$(VERSION)
	mypy --strict --scripts-are-modules --implicit-reexport $(MODULE) $(SCRIPTS)

check-code: .stamp-$(PROJECT)-$(VERSION)
	pylint --fail-under=$(PYLINT_SCORE) $(MODULE) $(SCRIPTS)

test: .stamp-$(PROJECT)-$(VERSION)
	pytest --doctest-modules -vv --cov=$(MODULE) $(MODULE)

.stamp-sphinx: .stamp-$(PROJECT)-$(VERSION)
	@grep -q $(PROJECT)-$(VERSION).pdf doc/index.rst || (echo documentation index does not point to ver. $(VERSION) && exit 1)
	sphinx-build doc build/doc
	sphinx-build -b latex doc build/latex
	make -C build/latex
	cp build/latex/$(PROJECT)-$(VERSION).pdf build/doc

.stamp-$(PROJECT)-$(VERSION): $(PKG_INFO)
	touch .stamp-$(PROJECT)-$(VERSION)

$(PKG_INFO): setup.cfg $(wildcard setup.py)
	python -m build -n -s -x

