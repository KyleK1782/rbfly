include .pymake.mk

SCRIPTS = scripts/rbfly-demo \
	scripts/rbfly-connect \
	scripts/rbfly-recv \
	scripts/rbfly-perf-send \
	scripts/rstream-perf-send
DOC_DEST = wrobell@dcmod.org:~/public_html/$(PROJECT)
PYLINT_SCORE = 7.6

# to include script utilities module
export MYPYPATH=scripts
